﻿// See https://aka.ms/new-console-template for more information
using NPL.M.A007.Exercise1;
string a = string.Format("{0,-20}{1,-20}{2,-20}{3,-20}", "ISBNNumber", "BookName", "AuthorName", "PublisherName");

Book book = new Book();
book.AuthorName = "J.K.Rowling";
book.PublisherName = "Kim Dong";
book.BookName = "Harry Potter";
book.ISBNNumber = 123456789;
Console.WriteLine(a);
Console.WriteLine(book.GetBookInformation());