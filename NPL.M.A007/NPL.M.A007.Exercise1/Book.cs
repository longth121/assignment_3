﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise1
{
    internal class Book
    {
        public string BookName { get; set; }
        public int ISBNNumber { get; set; }
        public string AuthorName { get; set; }
        public string PublisherName { get; set; }

        public Book(string bookName, int iSBNNumber, string authorName, string publisherName)
        {
            BookName = bookName;
            ISBNNumber = iSBNNumber;
            AuthorName = authorName;
            PublisherName = publisherName;
        }
        public Book() { }

        public string GetBookInformation()
        {

            return string.Format("{0,-20}{1,-20}{2,-20}{3,-20}",ISBNNumber,BookName,AuthorName,PublisherName);
        }
    }
}
