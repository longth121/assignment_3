﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Truck : Car
    {
        public Truck()
        {
        }

        public Truck(decimal speed, double regularPrice, string color, int weight) : base(speed, regularPrice, color)
        {
            this.Weight = weight;
        }

        public override double GetSalePrice()
        {
            if (Weight > 2000)
            {
                return RegularPrice - RegularPrice / 10;
            }
            else return RegularPrice - RegularPrice / 5;
        }

        public int Weight { get; set; }

        public void DisplayPrice()
        {
            Console.WriteLine("Truck price: " + GetSalePrice());
        }

    }
}
