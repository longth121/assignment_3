﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class MyOwnAutoShop
    {   
        Sedan sedan = new Sedan(150, 20000, "Green", 6);
        Ford ford1 = new Ford(140, 23000, "Black", 5, 1000);
        Ford ford2 = new Ford(130, 25000, "White", 4, 2000);
        Truck truck2 = new Truck(120, 20000, "Yellow", 4000);
        Truck truck1 = new Truck(100, 18000, "Pink", 3000);
        public void DisplaySalePrice()
        {
            sedan.DisplayPrice();
            ford1.DisplayPrice();
            ford2.DisplayPrice();
            truck1.DisplayPrice();
            truck2.DisplayPrice();
        }
    }

    
}
