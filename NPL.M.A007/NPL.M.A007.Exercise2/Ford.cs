﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Ford : Car
    {
        public Ford()
        {
        }

        public Ford(decimal speed, double regularPrice, string color, int year, int manufacturerDiscount) : base(speed, regularPrice, color)
        {
            this.Year = year;
            this.ManufacturerDiscount = manufacturerDiscount;
        }

        public int Year { get; set; }
        public int ManufacturerDiscount { get; set; }

        public override double GetSalePrice()
        {
            return RegularPrice - ManufacturerDiscount;
        }

        public void DisplayPrice()
        {
            Console.WriteLine("Price of Ford:" + GetSalePrice());
        }
    }
}
