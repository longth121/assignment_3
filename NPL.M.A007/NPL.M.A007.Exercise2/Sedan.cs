﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Sedan : Car
    {
        public Sedan()
        {
        }

        public Sedan(decimal speed, double regularPrice, string color, int lenght) : base(speed, regularPrice, color)
        {
            this.Length = lenght;
        }

        public int Length { get; set; }

        public override double GetSalePrice()
        {
            if (Length > 20)
            {
                return RegularPrice - RegularPrice / 40;
            }
            else return RegularPrice - RegularPrice / 20;
        }

        public void DisplayPrice()
        {
            Console.WriteLine("Sedan price: " + GetSalePrice());
        }
    }
}
